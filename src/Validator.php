<?php

namespace Task\MaturityValue;

class Validator
{
    private $mandatoryFields = [
        'policy_number',
        'policy_start_date',
        'premiums',
        'membership',
        'discretionary_bonus',
        'uplift_percentage'
    ];

    private $errors = [];

    private $isValid = true;

    /**
     * @param $row
     *
     * @return bool
     */
    public function isValidRow($row)
    {
        if (empty($row)) return false;

        $columnNameDiff = array_diff(array_keys($row), $this->mandatoryFields);

        if (! empty($columnNameDiff)) {
            $this->isValid = false;
            $this->errors[] = 'Invalid column headings: '. implode(', ', array_values($columnNameDiff));
        }


        foreach ($row as $key => $value) {
            if (empty($value)) {
                $this->isValid = false;
                $this->errors[] = $key . ' is empty';
            }
        }
        
        return $this->isValid;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
