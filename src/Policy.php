<?php
namespace Task\MaturityValue;

class Policy
{
    const MANAGEMENT_FEE_TYPE_A = 3;
    const MANAGEMENT_FEE_TYPE_B = 5;
    const MANAGEMENT_FEE_TYPE_C = 7;

    /**
     * @var int
     */
    private $policyNumber;
    /**
     * @var \DateTime
     */
    private $startDate;
    /**
     * @var bool
     */
    private $hasMembershipRight;
    /**
     * @var int
     */
    private $premiums;
    /**
     * @var int
     */
    private $DiscretionaryBonus;
    /**
     * @var float
     */
    private $UpliftPercentage;

    /**
     * @param string $policyNumber
     * @param \DateTime $startDate
     * @param bool $hasMembershipRight
     */
    public function __construct(string $policyNumber, \DateTime $startDate, bool $hasMembershipRight)
    {
        $this->policyNumber = $policyNumber;
        $this->startDate = $startDate;
        $this->hasMembershipRight = $hasMembershipRight;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime {
        return $this->startDate;
    }

    /**
     * @return string
     */
    public function getPolicyNumber(): string {
        return $this->policyNumber;
    }

    /**
     * @return bool
     */
    public function hasMembershipRight(): bool {
        return $this->hasMembershipRight;
    }

    /**
     * @return int
     */
    public function getPremiums(): int {
        return $this->premiums;
    }

    /**
     * @param int $premiums
     */
    public function setPremiums( int $premiums ) {
        $this->premiums = $premiums;
    }

    /**
     * @return int
     */
    public function getDiscretionaryBonus(): int {
        return $this->DiscretionaryBonus;
    }

    /**
     * @param int $DiscretionaryBonus
     */
    public function setDiscretionaryBonus( int $DiscretionaryBonus ) {
        $this->DiscretionaryBonus = $DiscretionaryBonus;
    }

    /**
     * @return float
     */
    public function getUpliftPercentage(): float {
        return ($this->UpliftPercentage / 100) + 1;
    }

    /**
     * @param float $UpliftPercentage
     */
    public function setUpliftPercentage( float $UpliftPercentage ) {
        $this->UpliftPercentage = $UpliftPercentage;
    }

    /**
     * @return int
     */
    public function getManagementFee()
    {
        if ($this->startsWith($this->policyNumber, 'A')) {
            return self::MANAGEMENT_FEE_TYPE_A;
        } elseif ($this->startsWith($this->policyNumber, 'B')) {
            return self::MANAGEMENT_FEE_TYPE_B;
        } elseif ($this->startsWith($this->policyNumber, 'C')) {
            return self::MANAGEMENT_FEE_TYPE_C;
        }
    }

    /**
     * @return int
     */
    public function getManagementFeeForPremium()
    {
        return $this->premiums * ($this->getManagementFee() / 100 );
    }

    /**
     * @param $haystack
     * @param $needle
     *
     * @return bool
     */
    private function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public function getMaturityValue()
    {
//        var_dump($this->premiums, $this->getManagementFee(), $this->getDiscretionaryBonus(), $this->getUpliftPercentage());
        return ($this->premiums - ($this->getManagementFeeForPremium()) + $this->getDiscretionaryBonus()) * $this->getUpliftPercentage();
    }

}
