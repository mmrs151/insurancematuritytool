<?php

namespace Task\MaturityValue;

class CsvParser
{
    private $models = [];
    /**
     * @var Validator
     */
    private $validator;
    /**
     * @var Builder
     */
    private $builder;
    /**
     * @var string
     */
    private $filePath;

    public function __construct(string $filePath, Validator $validator, Builder $builder)
    {
        $this->filePath = $filePath;
        $this->validator = $validator;
        $this->builder = $builder;
    }

    /**
     * @return array | null
     * @throws \Exception
     */
    public function parse()
    {
        ini_set('auto_detect_line_endings',TRUE);
        try{
            $file = fopen($this->filePath,'r');
            $header = fgetcsv($file); // take the header off from reading

            while ( ($row = fgetcsv($file) ) !== FALSE ) {
                $keyValue = array_combine($header, $row);
                $isValid = $this->validator->isValidRow($keyValue);
                if ($isValid) {
                    $this->models[] = $this->builder->getModel($keyValue);
                } else {
                    throw new \Exception(print_r($this->validator->getErrors(),1));
                }
            }
        } catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

        return $this->models;
    }
}
