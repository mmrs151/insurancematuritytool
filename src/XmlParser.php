<?php

namespace Task\MaturityValue;

use DOMDocument;

class XmlParser
{
    private $doc;

    public function __construct()
    {
        $this->doc = new DOMDocument('1.0');
        $this->doc->formatOutput = true;
    }

    /**
     * @param Policy[] $policies
     *
     * @return string
     */
    public function parse(array $policies, $outputFile)
    {
        $root = $this->doc->createElement('maturity');
        $root = $this->doc->appendChild($root);

        foreach ($policies as $policy) {

            $node = $this->doc->createElement('policy');
            $node = $root->appendChild($node);

            $policyNumberNode = $this->doc->createElement('policy_number');
            $policyNumberNode = $node->appendChild($policyNumberNode);

            $policyNumber = $this->doc->createTextNode($policy->getPolicyNumber());
            $policyNumber = $policyNumberNode->appendChild($policyNumber);

            $policyNumberNode = $this->doc->createElement('maturity_value');
            $policyNumberNode = $node->appendChild($policyNumberNode);

            $maturityValue = $this->doc->createTextNode($policy->getMaturityValue());
            $maturityValue = $policyNumberNode->appendChild($maturityValue);

        }

        $this->writeToFile($outputFile);
        return $this->doc->saveXML();
    }

    private function writeToFile($outputFile)
    {
        $outputFile .= '.xml';
        $this->doc->save($outputFile);
    }
}
