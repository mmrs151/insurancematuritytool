<?php

namespace Task\MaturityValue;

class Builder
{
    /**
     * @param array $data
     *
     * @return Policy
     */
    public function getModel(array $data)
    {
        $startDate = \DateTime::createFromFormat('d/m/Y', $data['policy_start_date']);
        $model = new Policy($data['policy_number'], $startDate, $data['membership']);
        $model->setPremiums($data['premiums']);
        $model->setDiscretionaryBonus($data['discretionary_bonus']);
        $model->setUpliftPercentage($data['uplift_percentage']);
        
        return $model;
    }
}
