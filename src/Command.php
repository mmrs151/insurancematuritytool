#!/usr/bin/php
<?php
namespace Task\MaturityValue;

require_once("CsvParser.php");
require_once("XmlParser.php");
require_once("Validator.php");
require_once("Builder.php");
require_once("Policy.php");

class Command
{
    public function execute($inputFile, $outputFile)
    {
        $csvParser = new CsvParser($inputFile, new Validator(), new Builder());
        $policies = $csvParser->parse();

        $xmlParser = new XmlParser();
        $xmlParser->parse($policies, $outputFile);
    }
}

$inputFile = __DIR__ .'/'. $argv[1];
$outputFile = __DIR__ .'/output/'. $argv[2];

$c = new Command();
$c->execute($inputFile, $outputFile);
