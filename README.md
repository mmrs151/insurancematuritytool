# To setup
`composer install`

# To test
`./vendor/bin/phpunit test/`

# To use the application
`./src/command.php input/MaturityData.csv result`

# To see the result
check for the `result.xml` in the `output` directory
