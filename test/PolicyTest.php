<?php
namespace Task\MaturityValue\Test;

use function PHPSTORM_META\type;
use Task\MaturityValue\Policy;

class PolicyTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Policy */
    private $maturity;

    public function setUp()
    {
        $policyNumber = 'A1001';
        $startDate = new \DateTime('2012-12-14');
        $this->maturity = new Policy($policyNumber, $startDate, true);
    }

    public function test_it_is_initialised()
    {
        $this->assertEquals('A1001', $this->maturity->getPolicyNumber());
        $this->assertEquals('2012-12-14', $this->maturity->getStartDate()->format('Y-m-d'));
        $this->assertEquals(true, $this->maturity->hasMembershipRight());
    }


    /**
     * @test
     */
    public function it_can_set_and_get()
    {
        $this->maturity->setPremiums(10000);
        $this->maturity->setDiscretionaryBonus(1350);
        $this->maturity->setUpliftPercentage(25);

        $this->assertEquals(10000, $this->maturity->getPremiums());
        $this->assertEquals(1350, $this->maturity->getDiscretionaryBonus());
        $this->assertEquals(1.25, $this->maturity->getUpliftPercentage());
    }

    /**
     * @test
     */
    public function it_can_calculate_correct_management_fee_for_type_A()
    {
        $this->assertEquals(3, $this->maturity->getManagementFee());
    }

    /**
     * @test
     */
    public function it_can_calculate_correct_management_fee_for_type_B()
    {
        $typeB = new Policy('B2002', new \DateTime(), true);

        $this->assertEquals(5, $typeB->getManagementFee());
    }

    /**
     * @test
     */
    public function it_can_calculate_correct_management_fee_for_type_C()
    {
        $typeC = new Policy('C2002', new \DateTime(), true);

        $this->assertEquals(7, $typeC->getManagementFee());
    }

    public function it_can_calculate_maturity_value()
    {
        $typeA = new Policy('A100001', new \DateTime('01/06/1986'), true);
        $typeA->setPremiums(10000);
        $typeA->setDiscretionaryBonus(1000);
        $typeA->setUpliftPercentage(40);

        $this->assertEquals(14980, $typeA->getMaturityValue());

    }
}
?>
