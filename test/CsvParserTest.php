<?php

namespace Task\MaturityValue\Test;

use Task\MaturityValue\Builder;
use Task\MaturityValue\CsvParser;
use Task\MaturityValue\Policy;
use Task\MaturityValue\Validator;

class CsvParserTest extends \PHPUnit_Framework_TestCase
{
    /** @var  CsvParser */
    private $parser;

    public function setUp() {
        $filePath = __DIR__.'/fixtures/invalid-sample.csv';
        $builder = new Builder();
        $validator = new Validator();
        $this->parser = new CsvParser($filePath, $validator, $builder);
    }

    /**
     * @expectedException \Exception
     */
    public function test_it_throws_exception_if_file_not_found()
    {
        $this->parser->parse();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Invalid column headings: bad_policy_number
     */
    public function test_it_throws_exception_for_wrong_data()
    {
        $filePath = __DIR__.'/fixtures/bad-sample.csv';
        $this->parser = new CsvParser($filePath, new Validator(), new Builder());
        $this->parser->parse();
    }

    public function test_it_returns_a_set_of_models()
    {
        $filePath = __DIR__.'/fixtures/sample.csv';
        $this->parser = new CsvParser($filePath, new Validator(), new Builder());

        /** @var Policy[] $result */
        $result = $this->parser->parse();
        $this->assertInstanceOf(Policy::class, $result[0]);
        $this->assertInstanceOf(Policy::class, $result[1]);
    }
}

