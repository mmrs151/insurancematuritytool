<?php

namespace Task\MaturityValue\Test;

use Task\MaturityValue\Validator;

class ValidatorTest extends \PHPUnit_Framework_TestCase
{
    public function test_it_can_validate_an_empty_row()
    {
        $validator = new Validator();

        $row = [];

        $this->assertEquals(false, $validator->isValidRow($row));
    }

    public function test_it_can_validate_an_empty_policy_number()
    {
        $validator = new Validator();

        $row = [
            'policy_number' => '',
            'policy_start_date' => '01/06/1986',
            'premiums' => 10000,
            'membership' => 'Y',
            'discretionary_bonus' => 1000,
            'uplift_percentage' => 40
        ];

        $this->assertEquals(false, $validator->isValidRow($row));
        $this->assertEquals(['policy_number is empty'], $validator->getErrors());
    }

    public function test_it_can_validate_if_multiple_value_is_missing()
    {
        $validator = new Validator();

        $row = [
            'policy_number' => 'A10001',
            'policy_start_date' => '01/06/1986',
            'premiums' => 0,
            'membership' => 'Y',
            'discretionary_bonus' => 1000,
            'uplift_percentage' => 0
        ];

        $this->assertEquals(false, $validator->isValidRow($row));
        $this->assertEquals(['premiums is empty', 'uplift_percentage is empty'], $validator->getErrors());
    }

    public function test_it_can_validate_wrong_column_heading()
    {
        $validator = new Validator();

        $row = [
            'policy_number' => 'A10001',
            'start_date' => '01/06/1986',
            'premiums' => 10,
            'membership' => 'Y',
            'bonus' => 1000,
            'uplift_percentage' => 10
        ];

        $this->assertEquals(false, $validator->isValidRow($row));
        $this->assertEquals(['Invalid column headings: start_date, bonus'], $validator->getErrors());
    }
}
