<?php

namespace Task\MaturityValue\Test;

use Task\MaturityValue\Policy;
use Task\MaturityValue\XmlParser;

class XmlParserTest extends \PHPUnit_Framework_TestCase
{
    private $outputFile;

    public function setUp()
    {
        $this->outputFile = __DIR__.'/fixtures/result';
    }

    public function test_it_can_create_xml_for_a_single_policy()
    {
        $policy = new Policy('A1010202', new \DateTime(), true);
        $policy->setDiscretionaryBonus(1000);
        $policy->setPremiums(10000);
        $policy->setUpliftPercentage(25);

        $policies[] = $policy;
        $parser = new XmlParser();
        $expected = "
        <maturity>
          <policy>
            <policy_number>A1010202</policy_number>
            <maturity_value>13375</maturity_value>
          </policy>
        </maturity>
        ";
        $this->assertXmlStringEqualsXmlString($expected, $parser->parse($policies, $this->outputFile));
    }

    public function test_it_can_create_xml_for_multiple_policy()
    {
        $policies = [];
        $policy1 = new Policy('A1010202', new \DateTime(), true);
        $policy1->setDiscretionaryBonus(1000);
        $policy1->setPremiums(10000);
        $policy1->setUpliftPercentage(40);

        $policy2 = new Policy('B3010202', new \DateTime(), true);
        $policy2->setDiscretionaryBonus(5000);
        $policy2->setPremiums(5000);
        $policy2->setUpliftPercentage(40);

        $policies[] = $policy1;
        $policies[] = $policy2;

        $parser = new XmlParser();
        $expected = "
        <maturity>
          <policy>
            <policy_number>A1010202</policy_number>
            <maturity_value>14980</maturity_value>
          </policy>
          <policy>
            <policy_number>B3010202</policy_number>
            <maturity_value>13650</maturity_value>
          </policy>
        </maturity>
        ";
        $this->assertXmlStringEqualsXmlString($expected, $parser->parse($policies, $this->outputFile));
    }
}
