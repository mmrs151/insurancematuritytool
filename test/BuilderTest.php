<?php
namespace Task\MaturityValue\Test;

use Task\MaturityValue\Builder;
use Task\MaturityValue\Policy;

class BuilderTest extends \PHPUnit_Framework_TestCase
{
    public function test_it_can_build_maturity_model()
    {
        $data = [
            'policy_number' => 'C303101',
            'policy_start_date' => '01/06/1986',
            'membership' => 'Y',
            'premiums' => 10000,
            'discretionary_bonus' => 1000,
            'uplift_percentage' => 40
        ];

        $builder = new Builder();
        $maturity = $builder->getModel($data);

        $this->assertInstanceOf(Policy::class, $maturity);
        $this->assertEquals('C303101', $maturity->getPolicyNumber());
        $this->assertEquals('1986-06-01', $maturity->getStartDate()->format('Y-m-d'));
        $this->assertEquals(true, $maturity->hasMembershipRight());
        $this->assertEquals(10000, $maturity->getPremiums());
        $this->assertEquals(1000, $maturity->getDiscretionaryBonus());
        $this->assertEquals(1.4, $maturity->getUpliftPercentage());
    }
}
?>
